import { Application, Router } from "https://deno.land/x/oak/mod.ts";
import { getBands, addBand } from "./controllers/bands.ts";

const app = new Application();
const router = new Router();
const PORT = 4000;

router
  .get("/bands", getBands)
  .post("/bands", addBand)

app.use(router.routes());

console.log(`Server running on Port: ${PORT}`);
await app.listen({ port: PORT });
