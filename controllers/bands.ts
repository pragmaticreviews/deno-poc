import { v4 } from 'https://deno.land/std/uuid/mod.ts';
import { Band } from '../types.ts'

let bands: Band[] = [
    {
      id: "1",
      name: "Metallica",
      genre: "Metal",
      website: "https://www.metallica.com"
    },
    {
      id: "2",
      name: "Ramones",
      genre: "Punk",
      website: "https://www.ramones.com"
    },
    {
      id: "3",
      name: "Marron 5",
      genre: "pop",
      website: "https://www.maroon5.com"
    },
];

// @desc    Get all bands
// @route   GET /bands
const getBands = ({ response }: { response: any }) => {
    response.body = {
        success: true,
        data: bands
    }
}

// @desc    Add band
// @route   Post /bands
const addBand = async ({ request, response }: { request: any, response: any }) => {    
    const body = await request.body()

    if (!request.hasBody) {
        response.status = 400
        response.body = {
            success: false,
            msg: 'The request is empty'
        }
    } else {
        const band: Band = body.value;
        band.id = v4.generate();
        bands.push(band);
        response.status = 201;
        response.body = {
            success: true,
            data: band
        }
    }
}

export { getBands,addBand };
